from influxdb import InfluxDBClient
from market_downloader import Market_Downloader
from datetime import datetime, timedelta

class StockDataBase():
    def __init__(self):
        self.db_client = InfluxDBClient(host='127.0.0.1', port=8086, username='kirilman', password='blue', database='stocks')

    def downloader_stocks_from_to(self,tickers, _from, to, interval):
        start_date = datetime.strptime(_from.split('+')[0], '%Y-%m-%dT%H:%M:%S.%f')
        end_date   = datetime.strptime(to.split('+')[0], '%Y-%m-%dT%H:%M:%S.%f')

        downloader = Market_Downloader()
        for ticker in tickers:#для каждого рынка
            start_date = datetime.strptime(_from.split('+')[0], '%Y-%m-%dT%H:%M:%S.%f')
            end_date   = datetime.strptime(to.split('+')[0], '%Y-%m-%dT%H:%M:%S.%f')
            for d in range((end_date - start_date).days): #для каждого торгового для 
                json_body = []
                from_start = (start_date+timedelta(d))
                tt = from_start.isoformat().split('T')[0]
                from_str = tt+'T06:00:00.00+00:00'
                to_str   = tt+'T23:00:00.00+00:00'
                candles = downloader.get_market_candles(ticker, from_str, to_str, interval) #свечи для инструмента за торговый день
                if candles is None:
                    continue
                # print(len(candles))
                for candle in candles:
                    body = {
                            "measurement":"stocks",
                            "tags":{
                                "ticker":ticker,
                                "interval":interval,
                            },
                            "time": candle.time.isoformat('T'),
                            "fields": {
                                'open': candle.o, 
                                'close': candle.c, 
                                'high':candle.h,
                                'low':candle.l,
                                'volum': candle.v,
                            },
                    }
                    
                    json_body.append(body)
                self.db_client.write_points(json_body)
            print(ticker+'  ', end='\r', flush=True)
tickers = ['GPS','SPR','MAC','UAL','GILD','AAL','AAPL','ABBV','AMD','APA','ATRA','BA','BABA','BBBY','BBY','CARS','CAT','CCI','CCK','CCL','CHEF','CVX','DAL','DIS','DK','DLPH','DLX','DOV','DXC','DY','EA','EBAY','EDIT','ET','EXC','EXEL','F','GILD','GIS','GOSS','GPN','GT','GTX','HA','HAL','HP','IBN','LB','LM','M','MO','MOMO','MRNA','MS','MSFT','NEM','O','OIS','OLL','OSIS','OVV','OXY','PAYC','PBF','PBI','PCG','PFG','PPG','PYPL','PZZA','QCOM','RCL',"RDS.A",'SAGE','SAIL','SAVE','SBUX','SNA','SNY','SPG','T','TSM','UAA','ULTA','UNH','WBA','WCC','WELL','WFC','WFC','XOM','ZYNE']
# tickers = ['AAL','AAPL']
# tickers = ['MSFT','NEM','O','OIS','OLL','OSIS','OVV','OXY','PAYC','PBF','PBI','PCG','PFG','PPG','PYPL','PZZA','QCOM','RCL','RDS.A','SAGE','SAIL','SAVE','SBUX','SNA','SNY','SPG','T','TSM','UAA','ULTA','UNH','WBA','WCC','WELL','WFC','WFC','XOM','ZYNE']
# tickers = ['RDS.A','SAGE']
StockDataBase().downloader_stocks_from_to(tickers, '2020-08-10T00:00:00.00',
                                                   '2020-09-04T23:00:00.00','30min')

#    '2020-05-26T23:00:00.00'
