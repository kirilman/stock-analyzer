from openapi_client import openapi
from datetime import datetime, timedelta
from pytz import timezone
from time import sleep
token = 't.8kIQjxNwbFIRFAPHmI9ybYD4-bqypMa6ssIJ8mjRo5XWksJnfhvz4zdW5bXPRKjfbLMN_sKSBDdtOFJx8VCJxA'
token = "t.wlnxazSuK6W--U6od2e6k778AW3nfY7MPm-1h93rzYXl6JIVPTnODfaeaJz1EjhfWl6XnJn2MCmIlqJPeeF6tg"

client = openapi.sandbox_api_client(token)
client.sandbox.sandbox_register_post()
client.sandbox.sandbox_clear_post()
# client.sandbox.sandbox_currencies_balance_post(sandbox_set_currency_balance_request={"currency": "USD", "balance": 1})


def set_balance():
    balance_set = client.sandbox.sandbox_currencies_balance_post({"currency": "RUB", "balance": 499999})
    print("balance")
    print(balance_set)
    print()


def print_24hr_operations():
    now = datetime.now(tz=timezone('Europe/Moscow'))
    yesterday = now - timedelta(days=1)
    ops = client.operations.operations_get(_from=yesterday.isoformat(), to=now.isoformat())
    print("operations")
    print(ops)
    print()


def print_orders():
    orders = client.orders.orders_get()
    print("active orders")
    print(orders)
    print()


def make_order():
    order_response = client.orders.orders_limit_order_post(figi='BBG00475K2X9',
                                                           limit_order_request={"lots": 62,
                                                                                "operation": "Buy",
                                                                                "price": 3.32})
    print("make order")
    print(order_response)
    print()
    return order_response


# won't work in sandbox - orders are being instantly executed
def cancel_order(order_id):
    cancellation_result = client.orders.orders_cancel_post(order_id=order_id)
    print("cancel order")
    print(cancellation_result)
    print()

def get_figi():
    tickers = ['MSNG','AGRO','POLY','RUAL','YNDX','QIWI','UNAC','DZRD','DZRDP','ISKJ','NAUK','LIFE','ALRS','ALNU','ABRD','AVAN','AKRN','APTK','ARSA','ASSB','AMEZ','AFLT','BSPB','BISV','BISVP','BANE','BANEP','BLNG','SYNG','ALBK','BRZL','VSMO','VTBR','VLHZ','VDSB','VJGZ','VJGZP','VZRZ','VZRZP','VGSB','VGSBP','VSYD','VSYDP','GAZA','GAZAP','GAZT','GAZS','GAZC','GAZP','GTSS','GRNT','GMKN','RTGZ','SIBN','HALS','FESH','DVEC','DASB','ZVEZ','ZILL','DIOD','RUSI','OPIN','IRKT','IGSTP','IGST','IDVP','IRAO','IRGZ','KMAZ','KZMS','KMEZ','KTSB','KTSBP','KUNF','KLSB','KCHE','KCHEP','TGKD','TGKDP','KSGR','KOGK','KMTZ','KROTP','KROT','KRSB','KRSBP','KUBE','KBTK','KUZB','KAZT','KAZTP','KGKC','KGKCP','LSRG','LKOH','LPSB','LVHK','LNZLP','LNZL','LNTA','LSNGP','LSNG','MVID','MGTSP','MGTS','MERF','CBOM','MAGN','MSRS','MRKZ','MRKK','MRKU','MRKP','MRKC','MRKV','MRKS','MRKY','MTSS','MAGE','MAGEP','MGNT','MFON','MFGS','MFGSP','ODVA','MTLR','MTLRP','MRSB','MORI','MOEX','MOBB','MSTT','MSST','NKNC','NKNCP','NKHP','NLMK','NMTP','NSVZ','NFAZ','NKSH','NVTK','UWGN','OGKB','UCSS','OMZZP','KZOS','KZOSP','GTLC','PIKK','PRTK','PAZA','PMSBP','PMSB','PLSM','PLZL','PRMB','RBCM','RGSS','RDRB','CHGZ','ROST','RASP','RLMN','RLMNP','ROSB','ROSN','RSTI','RSTIP','RTKM','RTKMP','AQUA','HYDR','RUGR','ROLO','RUSP','RZSB','SZPR','MGNZ','SVAV','SAGO','SAGOP','KRKN','KRKNP','SARE','SAREP','SBER','SBERP','CHMF','SELG','SELGP','SIBG','AFKS','JNOSP','JNOS','STSB','STSBP','SNGS','SNGSP','TANL','TANLP','TGKA','TGKN','TGKB','TGKBP','TUZA','KRKO','KRKOP','TUCH','TRMK','KBSB','MISBP','VRSBP','VRSB','MISB','NNSB','NNSBP','RTSB','RTSBP','YRSB','YRSBP','TNSE','TORS','TORSP','TASB','TASBP','TATN','TATNP','TTLK','CNTL','CNTLP','TRCN','TRNFP','URKZ','USBN','URKA','FEES','NPOF','PHOR','HIMC','HIMCP','WTCM','WTCMP','PRFN','CHKZ','CHMK','CHEP','GCHE','ELTZ','ENRU','RKKE','UTAR','UNKL','UKUZ','UPRO','YAKG','YKENP','YKEN','ENPL','ETLN','FIVE','RAVN','TCSG','GEMA','ACKO','FTRE','GTRK','DSKY','EELT','OBUV','RNFT','SFIN','SLEN','TRFM']

    #спб

    tickers = ['A','AA','AAL','AAN','AAP','AAPL','AAXN','ABBV','ABG','ABMD','ABT','ACAD','ACH','ACIA','ACM','ACN','ADBE','ADI','ADM','ADP','ADS','ADSK','ADUS','AEE','AEIS','AERI','AES','AFL','AGCO','AGIO','AGN','AIG','AIMT','AIR','AIV','AIZ','AJG','AJRD','AKAM','AKZM','ALB','ALFA0420EU','ALFA0421','ALFA0430','ALFAperp','ALFAperp0222','ALGN','ALGT','ALK','ALL','ALLE','ALLK','ALLO','ALNY','ALRM','ALSN','ALTR','ALV','ALXN','AMAT','AMCX','AMD','AME','AMED','AMG','AMGN','AMN','AMP','AMT','AMWD','AMZN','AN','ANAB','ANDE','ANET','ANGI','ANIK','ANIP','ANSS','ANTM','AOBC','AON','AOS','APA','APD','APEI','APH','APPF','APPN','APTV','APY','ARE','ARMK','ARNA','ARNC','ARW','ARWR','ASGN','ASH','ASIX','ATGE','ATKR','ATRA','ATRI','ATRO','ATUS','ATVI','AVAV','AVB','AVGO','AVLR','AVNS','AVY','AWI','AWR','AX','AXE','AXGN','AXP','AXSM','AYI','AYX','AZO','AZPN','BA','BABA','BAC','BAH','BAND','BAST','BAX','BBBY','BBSI','BBY','BC','BCO','BCPC','BDC','BDX','BEAT','BECN','BEL0627','BEN','BERY','BFAM','BH','BHF','BIDU','BIG','BIIB','BILI','BIO','BJRI','BK','BKI','BKNG','BKR','BL','BLD','BLDR','BLK','BLKB','BLL','BLUE','BMCH','BMI','BMRN','BMW','BMY','BOOT','BPMC','BR','BRKR','BSX','BTI','BUD','BURL','BWA','BXP','BYND','BZUN','C','CACC','CAG','CAH','CALM','CARA','CARG','CARS','CASY','CAT','CB','CBOM0224EU','CBPO','CBRE','CBRL','CCI','CCK','CCL','CDNA','CDNS','CE','CEA','CENT','CENTA','CERN','CF','CFG','CFX','CGNX','CHA','CHD','CHDN','CHE','CHEF','CHGG','CHK','CHL','CHMF1022','CHRW','CHTR','CI','CIEN','CINF','CL','CLF','CLGX','CLH','CLR','CLX','CMA','CMCO','CMCSA','CME','CMG','CMI','CMS','CNC','CNK','CNP','CNXN','COF','COG','COHR','COLM','COO','COP','CORT','COST','COTY','COUP','CPB','CPRI','CPRT','CPS','CR','CREE','CRI','CRL','CRM','CRMT','CROX','CRS','CRUS','CRVL','CSCO','CSGP','CSII','CSL','CSOD','CSWI','CTAS','CTB','CTL','CTLT','CTSH','CTVA','CTXS','CVCO','CVET','CVGW','CVLT','CVS','CVX','CXO','CY','CYOU','D','DAL','DAR','DBX','DD','DDS','DE','DECK','DFS','DG','DGX','DHI','DHR','DIOD','DIS','DISCA','DISCB','DISCK','DK','DKS','DLB','DLPH','DLR','DLTH','DLTR','DLX','DME0223','DNLI','DNOW','DOCU','DORM','DOV','DOW','DRI','DRQ','DVA','DVN','DXC','DXCM','DY','EA','EBAY','EBS','ECHO','ECL','ECPG','ED','EDIT','EEFT','EFX','EGHT','EGPT0329','EGPT0431','EGRX','EHTH','EIX','EL','ELAN','EMN','EMR','ENDP','ENSG','ENTA','ENV','EOG','EPAM','EPAY','EPC','EQIX','EQT','ERIE','ES','ESPR','ESS','ET','ETFC','ETN','ETR','ETRN','ETSY','EVBG','EVH','EVRZ0323','EW','EXAS','EXC','EXEL','EXLS','EXP','EXPD','EXPE','EXR','EYE','F','FANG','FARO','FAST','FATE','FB','FBHS','FCN','FCX','FDS','FDX','FFIV','FGEN','FICO','FIS','FISV','FITB','FIVE','FIVN','FIZZ','FL','FLIR','FLOW','FLR','FLS','FLT','FLWS','FMC','FND','FNKO','FOCS','FOE','FORM','FORR','FOX','FOXA','FOXF','FRHC','FRPH','FRPT','FSCT','FSLR','FSLY','FTI','FTNT','FTR','FTV','FUL','GAZP0322','GAZP0327','GAZP0837','GAZP1124','GBT','GBX','GCO','GCP','GD','GDDY','GDOT','GE','GEF','GH','GHC','GILD','GIS','GKOS','GL','GLPR0923','GLW','GM','GMED','GMKN1022','GMS','GNRC','GOOG','GOOGL','GOSS','GPBperp','GPC','GPI','GPN','GPS','GRA','GRMN','GRUB','GS','GSH','GSKY','GT','GTHX','GTLS','GTN','GTX','GVA','GWRE','GWW','H','HA','HAE','HAIN','HAL','HALO','HAS','HBAN','HBI','HCA','HCCI','HCSG','HD','HDS','HEAR','HES','HFC','HGV','HHC','HIBB','HIG','HII','HLT','HNP','HOG','HOLX','HON','HP','HPE','HPQ','HQY','HRB','HRL','HRTX','HSC','HSIC','HSKA','HST','HSY','HTHT','HUBG','HUBS','HUM','HURN','HXL','IAC','IART','IBM','IBN','IBP','ICE','ICUI','IDCC','IDXX','IFF','IIVI','ILMN','IMMU','INCY','INFO','INGN','INGR','INSP','INTC','INTU','IONS','IOVA','IP','IPAR','IPG','IPGP','IPHI','IQV','IR','IRBT','IRM','IRTC','ISBNK0424','ISRG','IT','ITGR','ITRI','ITW','IVZ','J','JBHT','JBSS','JCI','JCOM','JD','JEF','JELD','JKHY','JLL','JNJ','JNPR','JOBS','JOUT','JPM','JWN','K','KALU','KCEL','KDP','KEP','KEX','KEY','KEYS','KFY','KHC','KIM','KLAC','KMB','KMI','KMT','KMX','KNX','KO','KR','KSU','KTB','KZTK','L','LAD','LASR','LB','LEA','LECO','LEG','LEGH','LEN','LEVI','LFC','LGIH','LGND','LH','LHCG','LHX','LII','LIN','LITE','LKQ','LLY','LM','LMT','LNT','LNTH','LOGM','LOPE','LOW','LPL','LPSN','LRCX','LRN','LTHM','LUK0423','LUK1120','LUK1126','LULU','LUV','LVS','LYB','LYFT','LYV','M','MA','MAA','MAC','MAN','MANH','MANT','MANU','MAR','MAS','MASI','MAT','MATX','MBT','MBUU','MCD','MCHP','MCK','MCO','MCRI','MD','MDB','MDGL','MDLZ','MDRX','MDT','MEDP','MEI','MELI','MET','MFGP','MGLN','MGY','MHK','MHO','MIDD','MINI','MKC','MKL','MKTX','MLCO','MLHR','MLM','MMC','MMI','MMM','MMS','MMSI','MNK','MNST','MO','MOMO','MOS','MOV','MPC','MRC','MRK','MRNA','MRO','MRTX','MS','MSCI','MSFT','MSG','MSGN','MSI','MSM','MSTR','MTB','MTCH','MTD','MTG','MTH','MTN','MTOR','MTRN','MTS0620','MTSC','MU','MUR','MUSA','MXIM','MXL','MYGN','MYL','MYOK','MYRG','NAVI','NBIX','NBL','NCR','NDAQ','NDSN','NEE','NEM','NEO','NEOG','NEU','NEWR','NFLX','NGVT','NJR','NKE','NKTR','NLOK','NLSN','NMIH','NOC','NOK','NOV','NOW','NRG','NSC','NSIT','NSP','NTAP','NTCT','NTES','NTGR','NTLA','NTNX','NTRS','NTUS','NUE','NUS','NUVA','NVDA','NVEE','NVR','NVTA','NVTK0221','NWL','NWS','NWSA','NXST','O','OC','ODFL','OFIX','OI','OII','OIS','OKE','OKTA','OLED','OLLI','OMA0128','OMC','OMCL','ON','ONTO','ORCL','ORLY','OSIS','OSK','OSUR','OXY','PAGS','PANW','PATK','PAYC','PBCT','PBF','PBH','PBI','PCAR','PCG','PCRX','PCTY','PD','PDCO','PEAK','PEG','PEGA','PEN','PEP','PETQ','PFE','PFG','PFGC','PFPT','PG','PGR','PGTI','PH','PHM','PII','PINC','PINS','PKG','PKI','PLAN','PLAY','PLCE','PLD','PLNT','PLUS','PLXS','PLZL0223','PLZL0322','PM','PNC','PNTG','PODD','POL','POOL','POST','POWI','PPC','PPG','PPL','PRAA','PRAH','PRFT','PRGS','PRLB','PRSC','PRSP','PRU','PS','PSA','PSTG','PSX','PTC','PTR','PUMP','PVH','PWR','PXD','PYPL','PZZA','QADA','QCOM','QDEL','QLYS','QNST','QRTEA','QRVO','QTWO','QUOT','R','RACE','RAMP','RARE','RAVN','RCL','RDFN','RDY','RE','REG','REGI','REGN','RETA','REX','REZI','RF','RGEN','RGNX','RGR','RH','RHI','RIG','RJF','RL','RMD','RNG','ROCK','ROG','ROK','ROKU','ROL','ROLL','ROP','ROSN0322','ROST','RP','RPD','RPM','RRC','RRGB','RS','RSG','RSHB1023','RVLV','RXN','RYN','RYTM','RZD1020','SAFM','SAGE','SAIA','SAIL','SAM','SAVE','SBCF','SBER0222','SBGI','SBH','SBUX','SCCO','SCFL0623','SCHW','SCSC','SEDG','SEE','SEIC','SERV','SFIX','SFM','SGEN','SGENperp','SHAK','SHEN','SHI','SHW','SIG','SINA','SITE','SIVB','SJM','SKM','SKX','SLAB','SLB','SLG','SMAR','SMG','SMPL','SMTC','SNA','SNAP','SNBR','SNPS','SNX','SNY','SO','SOHU','SONO','SP','SPB@US','SPG','SPGI','SPLK','SPR','SPSC','SQ','SRCL','SRDX','SRE','SRI','SRPT','SSD','SSNC','SSTK','STAA','STLC0821','STLD','STRA','STT','STX','STZ','SUPN','SWAV','SWCH','SWI','SWK','SWKS','SWN','SXI','SXT','SYF','SYK','SYKE','SYNA','SYNH','SYY','T','TAK','TAL','TAP','TCBI','TCMD','TCRR','TCS','TCX','TDC','TDG','TDS','TDY','TECD','TEL','TENB','TER','TFC','TFX','TGNA','TGT','THO','THRM','THS','TIF','TJX','TKR','TMHC','TMO','TNDM','TNET','TOL','TOT','TPH','TPIC','TPR','TPX','TREE','TREX','TRIP','TRMB','TROW','TRU','TRUP','TRV','TRY0225','TRY0234','TSCO','TSLA','TSM','TSN','TTD','TTM','TTMI','TTWO','TWLO','TWNK','TWOU','TWTR','TXN','TXRH','TXT','TYL','UA','UAA','UAL','UBER','UCTT','UDR','UFPI','UFS','UHS','UI','ULTA','UNF','UNH','UNM','UNP','UNVR','UPS','UPWK','URBN','URI','USB','USFD','USM','USNA','UTHR','V','VAKI0324','VALE','VAR','VC','VCEL','VCRA','VCYT','VEEV','VEON','VFC','VG','VIAC','VICR','VIPS','VLO','VMC','VMW','VNDA','VNE','VNO','VPG','VREX','VRNS','VRNT','VRSK','VRSN','VRTU','VRTV','VRTX','VTB1020','VTBperp','VTR','VZ','W','WAB','WAL','WAT','WB','WBA','WBC','WCC','WDAY','WDC','WEC','WELL','WERN','WEX','WFC','WGO','WH','WHD','WHR','WING','WK','WLK','WLTW','WM','WMB','WMT','WOR','WRK','WRLD','WSM','WSO','WTS','WTTR','WU','WWD','WWE','WWW','WY','WYND','WYNN','XEC','XEL','XLNX','XLRN','XNCR','XOM','XPO','XRAY','XRX','XS0191754729','XS0304274599','XS0559915961','XS0848530977','XS0885736925','XS0893212398','XS0935311240','XS1319813769','XS1577953174','XS1603335610','XS1693971043','XS1752568144','XYL','Y','YELP','YETI','YEXT','YUM','YY','Z','ZBH','ZBRA','ZEN','ZG','ZGNX','ZION','ZM','ZNH','ZS','ZTS','ZUMZ','ZUO','ZYNE','ZYXI']


    tickers = ['ABRD','AFKS','AFLT','AKRN','ALBK','ALNU','ALRS','AMEZ','APTK','AQUA','ARSA','ASSB','AVAN','BANE','BANEP','BELU','BISV','BISVP','BLNG','BRZL','BSPB','CBOM','CHEP','CHGZ','CHKZ','CHMF','CHMK','CNTL','CNTLP','DASB','DIOD','DSKY','DVEC','DZRD','DZRDP','ELTZ','ENPG','ENRU','FEES','FESH','GAZA','GAZAP','GAZC','GAZP','GAZS','GAZT','GCHE','GEMA','GMKN','GTRK','GTSS','HALS','HIMC','HIMCP','HYDR','IDVP','IGST','IGSTP','INGR','IRAO','IRGZ','IRKT','ISKJ','JNOS','JNOSP','KAZT','KAZTP','KBSB','KBTK','KCHE','KCHEP','KGKC','KGKCP','KLSB','KMAZ','KMEZ','KMTZ','KOGK','KRKN','KRKNP','KRKO','KRKOP','KROT','KROTP','KRSB','KRSBP','KSGR','KTSB','KTSBP','KUBE','KUNF','KUZB','KZMS','KZOS','KZOSP','LIFE','LKOH','LNZL','LNZLP','LPSB','LSNG','LSNGP','LSRG','LVHK','MAGE','MAGEP','MAGN','MFGS','MFGSP','MFON','MGNT','MGNZ','MGTS','MGTSP','MISB','MISBP','MOBB','MOEX','MORI','MRKC','MRKK','MRKP','MRKS','MRKU','MRKV','MRKY','MRKZ','MRSB','MSNG','MSRS','MSTT','MTLR','MTLRP','MTSS','MVID','NAUK','NFAZ','NKHP','NKNC','NKNCP','NKSH','NLMK','NMTP','NNSB','NNSBP','NPOF','NSVZ','NVTK','OBUV','OGKB','OMZZP','PAZA','PHOR','PIKK','PLZL','PMSB','PMSBP','PRMB','PRTK','RASP','RBCM','RDRB','RGSS','RKKE','RLMN','RLMNP','RNFT','ROLO','ROSB','ROSN','ROST','RSTI','RSTIP','RTGZ','RTKM','RTKMP','RTSB','RTSBP','RUGR','RUSI','RUSP','RZSB','SAGO','SAGOP','SARE','SAREP','SBER','SBERP','SELG','SELGP','SFIN','SIBN','SLEN','SNGS','SNGSP','STSB','STSBP','SVAV','SZPR','TASB','TASBP','TATN','TATNP','TGKA','TGKB','TGKBP','TGKD','TGKDP','TGKN','TNSE','TORS','TORSP','TRCN','TRFM','TRMK','TRNFP','TTLK','TUZA','UCSS','UKUZ','UNAC','UNKL','UPRO','URKA','URKZ','USBN','UTAR','UWGN','VGSB','VGSBP','VJGZ','VJGZP','VLHZ','VRSB','VRSBP','VSMO','VSYD','VSYDP','VTBR','VZRZ','WTCM','YKENP','YRSB','YRSBP','ZILL','ZVEZ','AGRO','ENPL','ETLN','FIVE','LNTA','POLY','QIWI','RAVN','RUAL','TCSG','YNDX']
    c = 0
    tickers = ["DAL" ,"BA" ,"OII" ,"PLZL","RDS.A","NEM" ,"OIS" ,"GILD","SNY" ,"SPR" ,"APY" ,"TSM" ,"AMD" ,"CCL" ,"MNK" ,"SPG" ,"PFE" ,"PLAY","WFC" ,"PCG" ,"WELL","GTX" ,"KR","XOM" ,"INTC","MRNA","MSFT","MOMO","V" ,"MU" ,"PBI" ,"OVV" ,"EVH" ,"RIG" ,"ET" ,"ZYNE","ATRA","TSLA","PBF" ,"PEP" ,"TGNA","APA" ,"FCX" ,"ULTA"]
    
    tickers = ['SPX', 'SPY', 'US500','SP500','S&P','500','^GSPC']
    f = open('figi_spb.txt','w')
    # tickers = ['ISKJ','POLY','ISKJ']
    for ticker in tickers:
        thread = client.market.market_search_by_ticker_get_with_http_info(ticker, async_req=True)
        try:
            result = thread.get()[0].to_dict()
            
        except:
            c+=1
            continue
        if len(result['payload']['instruments'])==0:
            print(ticker)
            c+=1
            continue
        figi = result['payload']['instruments'][0]['figi']
        f.write(ticker+';'+figi+'\n')
        sleep(0.3)
    f.close()

    print("Неудалось скачать ", c)

from FIGIS import Market_names

get_figi()


token = "t.wlnxazSuK6W--U6od2e6k778AW3nfY7MPm-1h93rzYXl6JIVPTnODfaeaJz1EjhfWl6XnJn2MCmIlqJPeeF6tg"
client = openapi.sandbox_api_client(token)
client.sandbox.sandbox_register_post()
market_api = client.market
_from = '2019-08-19T18:38:33.131642+03:00'
to = '2019-08-19T19:38:33.131642+03:00'

figi= 'BBG007JVG1B3 '
ans = market_api.market_candles_get(figi, _from, to, '1min')
