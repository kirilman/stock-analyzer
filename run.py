from candle import CandleStream
import time
from FIGIS import Market_names
FIGIS = Market_names()
# from openapi_client.openapi_streaming import reconnect
# print(reconnect)
import websocket
# candle_subs = [ 
#                 # {'figi': 'BBG000BBQCY0', 'interval': '1min'}, 
#                 # {'figi': 'BBG000C0G1D1', 'interval': '1min'},
#                 {'figi': 'BBG000F2HWW4', 'interval': '1min'},
#                 {'figi': 'BBG00475K2X9', 'interval': '1min'},    
#                 {'figi': 'BBG006L8G4H1', 'interval': '1min'},
#             ]
# candle_subs = [ 
#                 # {'figi': 'BBG000BBQCY0', 'interval': '1min'}, 
#                 # {'figi': 'BBG000C0G1D1', 'interval': '1min'},
#                 {'figi': FIGIS.get_figi_by_name(['HYDR']), 'interval': '1min'},
#                 {'figi': FIGIS['SBER'], 'interval': '1min'},    
#                 # {'figi': FIGIS['ROSN'], 'interval': '1min'},
#                 # {'figi': FIGIS['YNDX'], 'interval': '1min'},    
#                 # {'figi': FIGIS['IRAO'], 'interval': '1min'},
#                 # {'figi': FIGIS['TGKD'], 'interval': '1min'},
#                 # {'figi': FIGIS['MAGN'], 'interval': '1min'},    
#                 # {'figi': FIGIS['CHEP'], 'interval': '1min'},
#                 {'figi': FIGIS['RSTI'], 'interval': '1min'},    
#                 {'figi': FIGIS['CHMF'], 'interval': '1min'},
#                 {'figi': FIGIS['TGKA'], 'interval': '1min'},
            # ]

candle_subs = [ {'figi':FIGIS.get_figi_by_name('YNDX'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('MSNG'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('AGRO'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('POLY'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('RUAL'),'interval': '1min'},
                
                {'figi':FIGIS.get_figi_by_name('QIWI'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('UNAC'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('ALRS'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('ALNU'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('ABRD'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('AKRN'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('APTK'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('AMEZ'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('AFLT'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('BSPB'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('BANE'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('BANEP'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('BLNG'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('BRZL'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('VSMO'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('VTBR'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('VZRZP'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('GAZP'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('GRNT'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('GMKN'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('SIBN'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('HALS'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('FESH'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('DVEC'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('DASB'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('DIOD'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('OPIN'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('IRKT'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('IRAO'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('IRGZ'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('KMAZ'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('KLSB'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('TGKD'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('KROT'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('KBTK'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('LSRG'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('LKOH'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('LNZL'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('LNTA'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('LSNGP'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('LSNG'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('MVID'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('MGTSP'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('CBOM'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('MAGN'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('MSRS'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('MRKZ'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('MRKU'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('MRKP'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('MRKC'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('MRKV'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('MRKS'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('MRKY'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('MTSS'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('MGNT'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('MTLR'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('MTLRP'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('MOEX'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('MSTT'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('MSST'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('NKNC'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('NKNCP'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('NKHP'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('NLMK'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('NMTP'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('NSVZ'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('NVTK'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('UWGN'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('OGKB'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('KZOS'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('KZOSP'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('PIKK'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('PRTK'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('PMSBP'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('PMSB'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('PLZL'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('RBCM'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('ROST'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('RASP'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('ROSN'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('RSTI'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('RSTIP'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('RTKM'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('RTKMP'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('HYDR'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('RUGR'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('ROLO'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('RUSP'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('SVAV'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('KRKNP'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('SBER'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('SBERP'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('CHMF'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('SELG'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('AFKS'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('SNGS'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('SNGSP'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('TGKA'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('TGKN'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('TGKB'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('TGKBP'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('TRMK'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('VRSB'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('TATN'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('TATNP'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('TTLK'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('CNTL'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('CNTLP'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('TRCN'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('TRNFP'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('FEES'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('PHOR'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('PRFN'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('CHMK'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('CHEP'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('GCHE'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('ENRU'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('RKKE'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('UNKL'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('UPRO'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('ENPL'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('FIVE'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('RAVN'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('TCSG'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('GTRK'),'interval': '1min'},
                {'figi':FIGIS.get_figi_by_name('DSKY'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('OBUV'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('RNFT'),'interval': '1min'},
                # {'figi':FIGIS.get_figi_by_name('SFIN'),'interval': '1min'},
            ]

orderbook_subs = []
instrument_info_subs = [
                        # {'figi': 'BBG000BBQCY0'}, 
                        # {'figi': 'BBG000C0G1D1'}, 
                        # {'figi': 'BBG000DD0BC8'},
                        # {'figi': 'BBG00475K2X9'},
                        # {'figi': 'BBG006L8G4H1'}
                        ]

stream = CandleStream(candle_subs, orderbook_subs, instrument_info_subs)
stream.setDaemon(True)
stream.start()
time.sleep(3)
c = 0
while True:
    c+=1
    if c%100==0:
        print(stream._candles_map._candles_map[-1][0], stream.ws.sock, stream.ws)
    time.sleep(0.01)
    
    if c%500==0:
        if stream.ws.sock:
            stream.ws.sock.close()
            print('Поток закрыт')
        else:
            print('Попытка перезапуска')
            # stream.ws.sock
            try:
                stream.ws.run_forever()
            except Exception as e:
                print('Исключение', e)
            print('Поток открыт')
#     print('close')
# time.sleep(2)
# print(stream.ws)
# time.sleep(3)
# stream.ws.run_forever()
