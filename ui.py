# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets, Qt
from canvas_figure import Canvas

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(740, 581)
        # self.centralwidget = QtWidgets.QWidget(MainWindow)
        # self.centralwidget.setObjectName("centralwidget")
       
        self.tab = Qt.QTabWidget()
        #Вкладка №1
        self.container_page_1 = QtWidgets.QWidget()
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout1 = QtWidgets.QHBoxLayout()
        self.horizontalLayout1.setObjectName("horizontalLayout1")  
        self.verticalLayout.addLayout(self.horizontalLayout1)
        
        self.container_page_1.setLayout(self.verticalLayout)
        #Вкладка №2
        self.container_page_2 = QtWidgets.QWidget()
        self.horizontalLayout_page_2 = QtWidgets.QHBoxLayout()
        self.slider = QtWidgets.QSlider(QtCore.Qt.Vertical)
        self.slider.setValue(1)
        self.slider.setTickPosition(QtWidgets.QSlider.TicksBelow)
        self.slider.setTickInterval(1)

        self.correlation_figure = Canvas(3,3, dpi=100)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        self.correlation_figure.setSizePolicy(sizePolicy)
        self.horizontalLayout_page_2.addWidget(self.correlation_figure)
        self.vertical_box = QtWidgets.QVBoxLayout()
        self.label_current_date = QtWidgets.QLabel('Дата')
        self.label_begin = QtWidgets.QLabel('Начальная дата')
        self.label_end = QtWidgets.QLabel('Конечная дата')

        # self.label_begin.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Minimum))
        # self.label_end.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Minimum))
        # self.slider.setSizePolicy(QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Minimum)
        # self.horizontalLayout_page_2.addWidget(self.slider)
        self.vertical_box.addWidget(self.label_current_date)
        self.vertical_box.addWidget(self.label_end)
        self.vertical_box.addWidget(self.slider)
        self.vertical_box.addWidget(self.label_begin)
    
        
        self.horizontalLayout_page_2.addLayout(self.vertical_box)
        self.container_page_2.setLayout((self.horizontalLayout_page_2))
        

        #Вкладка 3
        def createTable():
            
            table = QtWidgets.QTableWidget()
            table.setColumnCount(12)
            table.setRowCount(50)
            table.setItem(0,0, QtWidgets.QTableWidgetItem("Маркет"))
            table.setItem(0,1, QtWidgets.QTableWidgetItem("Изменение"))
            table.setItem(0,2, QtWidgets.QTableWidgetItem("Маркет"))
            table.setItem(0,3, QtWidgets.QTableWidgetItem("Изменение"))
            table.setHorizontalHeaderLabels(["Маркет", "Изменение", "Маркет", "Изменение","Маркет", "Изменение", "Маркет", "Изменение",
                                             "Маркет","Угол плж", "Маркет","Угол отр"])
            # table.setFixedWidth(40)
            for i in range(table.rowCount()):
                table.setRowHeight(i,16)

            for column in range(table.columnCount()):
                table.setColumnWidth(column, 70)
            return table

        self.container_page_3 = QtWidgets.QWidget()
        self.horizontalLayout_page_3 = QtWidgets.QHBoxLayout()
        self.button = QtWidgets.QPushButton('Очистить')
        self.button.setFixedWidth(40)
        self.table_stocks = createTable()
        self.text_edit = QtWidgets.QPlainTextEdit('')
        self.text_edit.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.text_edit.setFixedWidth(600)
        print(self.text_edit.width(), self.text_edit.height())
        
        self.horizontalLayout_page_3.addWidget(self.table_stocks)
        self.horizontalLayout_page_3.addWidget(self.text_edit)    
        self.horizontalLayout_page_3.addWidget(self.button)
        self.container_page_3.setLayout((self.horizontalLayout_page_3))


        self.tab.addTab(self.container_page_1,'Изменение объемов')
        self.tab.addTab(self.container_page_2,'Корреляционная карта')
        self.tab.addTab(self.container_page_3,'Сигналы для изменения объемов')


        MainWindow.setCentralWidget(self.tab)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 740, 23))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))

