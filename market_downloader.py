import os
from datetime import datetime, timedelta
# from urllib.parse import urlencode
# from urllib.request import urlopen
from time import sleep
import time
from multiprocessing import Process
from urllib.error import HTTPError
from openapi_genclient import exceptions

from openapi_client import openapi
from FIGIS import Market_names

class Market_Downloader():
    def __init__(self):
        self.token = "t.wlnxazSuK6W--U6od2e6k778AW3nfY7MPm-1h93rzYXl6JIVPTnODfaeaJz1EjhfWl6XnJn2MCmIlqJPeeF6tg"
        client = openapi.sandbox_api_client(self.token)
        client.sandbox.sandbox_register_post()
        self.market_api = client.market
        self.market_names = Market_names()

    def download_market_candles(self, save_dir, ticker, _from, to, interval):
        """
            _from : 2019-08-19T18:38:33.131642+03:00
            interval: ("1min","5min","day")
        """
        figi = self.market_names.get_figi_by_name(ticker)
        print(figi)
        if figi is None:
            print('Не найден FIGI для', ticker)
            return 
        ans = self.market_api.market_candles_get(figi, _from, to, interval)
        candles = ans.payload.candles
        full_path = save_dir + ticker+'.txt'
        f = open(full_path,'w')
        f.write('<OPEN>;<HIGH>;<LOW>;<CLOSE>;<VOL>\n')
        for candle in candles:
            close = candle.c
            high  = candle.h
            low   = candle.l
            open_ = candle.o
            volum = candle.v
            f.write('{};{};{};{};{}\n'.format(open_, high, low, close, volum))
        f.close()
        # print(candles)
    
    def get_market_candles(self, ticker, _from, to, interval):
        """
            Получить свечи для инструмента с заданный шагом
        """
        figi = self.market_names.get_figi_by_name(ticker)
        if figi is None:
            print('Не найден FIGI для', ticker)
            return None
        try:
            ans = self.market_api.market_candles_get(figi, _from, to, interval)
        except Exception as exc:
            if exc.status == '429':
                sleep(5)
                self.get_market_candles(ticker, _from, to, interval)
            # print(dir(ans))
            return None
        candles = ans.payload.candles
        if candles is not None:
            return candles
        else:
            return None


    def download_markets_from_to(self, save_dir, tickers, _from, to, interval):
        for ticker in tickers:
            try:
                self.download_market_candles(save_dir, ticker, _from, to, interval)
            except:
                print(ticker)
                sleep(3)
        
    def has_last_stocks(self, path):
        dirs = os.listdir(path)
        current_time = datetime.now() - timedelta(days=1)
        # current_time_str = current_time.strftime('%d.%m.%Y')
        day = current_time.day
        day_str = '{}'.format(day)
        if not(day_str in dirs):
            return False, [], day_str
        else:
            return True, os.listdir(path+day_str), day_str

    def get_last_day(self, tickers, market_names:Market_names):
        msk = '+03:00'
        path = './stocks last day/'

        token = "t.wlnxazSuK6W--U6od2e6k778AW3nfY7MPm-1h93rzYXl6JIVPTnODfaeaJz1EjhfWl6XnJn2MCmIlqJPeeF6tg"
        now = datetime.now()
        if now.weekday() == 0 and now.hour < 14:
            start = now - timedelta(hours = now.hour, minutes = now.minute, seconds = now.second) - timedelta(days=3)
        else:
            start = now - timedelta(hours = now.hour, minutes = now.minute, seconds = now.second) - timedelta(days=1)

        start_str = start.isoformat() + msk
        end_str = (start+timedelta(days=1)).isoformat()+msk
        
        print(start_str, end_str)
        client = openapi.sandbox_api_client(token)
        client.sandbox.sandbox_register_post()   
        def get_tickers(tickers, tickers_in_directory, day):
            minus = list(set(tickers) - set(tickers_in_directory))
            print(len(minus))
            for ticker in minus:
                sleep(0.15)
                figi = market_names.get_figi_by_name(ticker)
                # print(figi)
                if figi is None:
                    print('figi не получен ',ticker)
                    continue
                try:
                    response = client.market.market_candles_get(figi,start_str,end_str,'day')
                except exceptions.ApiException as err:
                    print('Исключение ',ticker,' ',err.reason, ' ',err.status)
                    continue
                # if response.status=="Error":
                #     continue
                candles = response._payload._candles
                if len(candles) > 1:
                    print('Получено свечей больше чем на один день!', len(candles))
                    candle = candles[-1]
                elif len(candles) == 0:
                    print('Длина ', len(candles), ticker)
                    continue
                else:
                    candle = candles[0]
                close = candle.c
                high  = candle.h
                low   = candle.l
                open_ = candle.o
                volum = candle.v
                full_path = path + day + '/' + ticker+'.txt'
                
                f = open(full_path,'w')
                f.write('<OPEN>;<HIGH>;<LOW>;<CLOSE>;<VOL>\n')
                f.write('{};{};{};{};{}'.format(open_, high, low, close, volum))
                f.close()
                
                # print(ticker, ' готов')

        flag, files, day = self.has_last_stocks(path)
        # print(flag, files, day)
        files = [x.split('.')[0] for x in files]
        try:
            if flag:
                get_tickers(tickers, files, day)
            else:
                os.mkdir(path+day)
                get_tickers(tickers, files, day)
        except Exception as exc:
            print('Error', exc, type(exc))
        return path + day + '/'


# m_loader = Market_Downloader()
# m_loader.download_market_candles('./Downloader/SPB/','AMD', '2020-05-13T16:30:00.00+04:00','2020-05-14T05:50:00.00+04:00', '5min')
# tickers = ['AMD','CCL','JPM','INTEL','OIS','AAL','DLT']

# m_loader.download_markets_from_to('./Downloader/SPB/',tickers, '2020-05-07T16:30:00.00+04:00','2020-05-14T12:40:00.00+04:00', 'hour')