import pandas as pd
import os
import csv
import datetime
import ciso8601
import numpy as np
class Reader():

    def __init__(self):
        pass

    def read_directory_as_frame(self, directory_name, **arg):
        files = os.listdir(directory_name)
        frames = []
        for current_file in files:
            frame = pd.read_csv(directory_name+'/'+current_file, **arg)
            frames.append(frame)
        return frames

    def read_file(self, file_name):
            csv_file = open(file_name, 'r',newline='')
            reader = csv.reader(csv_file)             #Date	Open	High	Low	Close	Volume
            header = next(reader)
            data = []
            times = []
            data_close = []

            for line in reader:
                elems = line[0].split(';')
                if '/' in elems[0]:
                    t = datetime.datetime.strptime(elems[0],'%d/%m/%y')
                else:
                    t = ciso8601.parse_datetime(elems[0])

                data.append(float(elems[1]))
                data_close.append(float(elems[2]))
                times.append(t)
            return np.array(data), np.array(data_close), times

    def create_frame(self, directory_name):
        list_names = os.listdir(directory_name)
        frame = pd.DataFrame()
        for name in list_names:
            opens, _, times = self.read_file(directory_name + '/'+ name)
            colum_name = name.split('.')[0]
            series = pd.Series(data=opens, index=times)
            frame[colum_name] = series
        frame.to_csv('MOEX_TOP.csv')

    def read_frame(self, file_name, **arg):
        print(str(arg))
        frame = pd.read_csv(file_name, **arg)
        frame.index = pd.to_datetime(frame.index)
        frame['month'] = frame.index.month + ((frame.index.year - frame.index[0].year)*12) - frame.index[0].month + 1 #индекс месяца
        return frame



# read = Reader()
# read.create_frame('stocks')