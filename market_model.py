import numpy as np
from influxdb import InfluxDBClient
from market_downloader import Market_Downloader
from datetime import datetime, timedelta

class Market_model():
    def __init__(self, selected_tickers):
        self.db_client = InfluxDBClient(host='127.0.0.1', port=8086, username='kirilman', password='blue')
        self.__selected_tickers = selected_tickers

    #Обновиться статистики
    def wrire(self, f,ticker, ans):
        f.write(ticker+str(ans)+'\n')

    def update_tickers_statics(self):
        # self.db_client.query("DELETE FROM statics;",database='statics')
        for ticker in self.__selected_tickers:
            statics = {}
            price = 0.0
            round_price = -1
            procent_to_round = -1
            for last_days in [2,7,30,90]:
                ans = self.db_client.query("SELECT * FROM stocks WHERE ticker = '{}' and time > now() - {}d;".format(ticker, last_days), 
                                          database='stocks')
                if len(ans) == 0:  
                    statics[last_days] = {'max':0.01, 'min':0.01}

                    continue
                candles = list(ans)[0]
               
                closes = np.array([c['close'] for c in candles])
                max_c = closes.max()
                min_c = closes.min()
                if (max_c is None) or (min_c is None):
                    max_c = 0.01
                    min_c = 0.01
                statics[last_days] = {'max':max_c, 'min':min_c}
                price = closes[-1]
                if last_days == 2:
                    round_price = int(np.round(price))
                    procent_to_round = (round_price - price)/price*100 
                
            body = {
                "measurement":"statics",
                "tags":{
                    "ticker":ticker,
                },
                "fields": {
                    'max_day'         : statics[2]['max'],
                    'min_day'         : statics[2]['min'],
                    'max_week'        : statics[7]['max'],
                    'min_week'        : statics[7]['min'],
                    'max_month'       : statics[30]['max'],
                    'min_month'       : statics[30]['min'],
                    'max'             : statics[90]['max'],
                    'min'             : statics[90]['min'],
                    'price'           : price,
                    'round_price'     : round_price,
                    'procent_to_round': float(round(procent_to_round,2)),
                },
            },
            try:
                # print(body)
                self.db_client.write_points(body, database ='statics')
            except Exception as exc:
                print(exc)
        
                
tickers = ['AAL','AAPL','ABBV','AMD','APA','ATRA','BA','BABA','BBBY','BBY','CARS','CAT','CCI','CCK','CCL','CHEF','CVX','DAL','DIS','DK','DKS','DLPH','DLX','DOV','DXC','DY','EA','EBAY','EDIT','ET','EXC','EXEL','F','GILD','GIS','GOSS','GPN','GT','GTX','HA','HAL','HP','IBN','LB','LM','M','MO','MOMO','MRNA','MS','MSFT','NEM','O','OIS','OLL','OSIS','OVV','OXY','PAYC','PBF','PBI','PCG','PFG','PPG','PYPL','PZZA','QCOM','RCL',"RDS.A",'SAGE','SAIL','SAVE','SBUX','SNA','SNY','SPG','T','TSM','UAA','ULTA','UNH','WBA','WCC','WELL','WFC','XOM','ZYNE']

market = Market_model(tickers)
market.update_tickers_statics()