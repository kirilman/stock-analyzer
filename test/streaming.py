from openapi_client.openapi_streaming import run_stream_consumer
from openapi_client.openapi_streaming import print_event


token = "t.8kIQjxNwbFIRFAPHmI9ybYD4-bqypMa6ssIJ8mjRo5XWksJnfhvz4zdW5bXPRKjfbLMN_sKSBDdtOFJx8VCJxA"
#
candle_subs = [{'figi': 'BBG000BBQCY0', 'interval': '1min'}]
# candle_subs = [{'figi': 'BBG000C9R1H6', 'interval': '1min'}]

# orderbook_subs = [{'figi': 'BBG0013HGFT4', 'depth': 1}, {'figi': 'BBG009S39JX6', 'depth': 5}]
# instrument_info_subs = [{'figi': 'BBG000B9XRY4'}, {'figi': 'BBG009S39JX6'}]
instrument_info_subs = [{'figi': 'BBG000BBQCY0'}]

# run_stream_consumer(token,
#                     candle_subs, orderbook_subs, instrument_info_subs,
#                     on_candle_event=print_event,
#                     on_orderbook_event=print_event,
#                     on_instrument_info_event=print_event)


run_stream_consumer(token,
                    candle_subs, #orderbook_subs, 
                    instrument_info_subs= instrument_info_subs,
                    on_candle_event=print_event,
                    on_orderbook_event=print_event,
                    on_instrument_info_event=print_event)
