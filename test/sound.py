from multiprocessing import Pool
from time import sleep
def f(x):
    r = str(x) + '1'
    return r 

if __name__ == '__main__':
    p = Pool(5)
    print(p.map(f, range(200)))