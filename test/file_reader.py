import datetime
import ciso8601
import csv
import numpy as np

class FileReader:
    def __init__(self):
        pass

    def read_file(self, file_name):
        csv_file = open(file_name, 'r',newline='')
        reader = csv.reader(csv_file)             #Date	Open	High	Low	Close	Volume
        header = next(reader)
        data = []
        times = []
        data_close = []

        for line in reader:
            elems = line[0].split(';')
            if '/' in elems[0]:
                t = datetime.datetime.strptime(elems[0],'%d/%m/%y')
            else:
                t = ciso8601.parse_datetime(elems[0])

            data.append(float(elems[1]))
            data_close.append(float(elems[2]))
            times.append(t)
        return np.array(data), np.array(data_close), times