from django.urls import path
from . import views


urlpatterns = [
    path('', views.stocks_table, name='stocks_table'),
]