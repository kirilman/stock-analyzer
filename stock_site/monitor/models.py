from django.db import models

# Create your models here.


class TickerState(models.Model):
    ticker = models.CharField(max_length=100,verbose_name='Ticker name')
    current_price = models.FloatField(null=True)
    price_max_week = models.FloatField(null=True)
    