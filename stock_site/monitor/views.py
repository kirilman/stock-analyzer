from django.shortcuts import render
import json
# Create your views here.
from influxdb import InfluxDBClient
import django_tables2 as tables
from django.utils.html import format_html

client = InfluxDBClient(host='127.0.0.1', port=8086, username='kirilman', password='blue')

class NameTable(tables.Table):
    ticker = tables.Column()
    
    min_day   = tables.Column()
    min_week  = tables.Column()
    min_month = tables.Column()
    
    max_day = tables.Column()
    max_week  = tables.Column()
    max_month = tables.Column()

    min_prnt  = tables.Column()
    max_prnt    = tables.Column()
    round_price = tables.Column(verbose_name='Round\n Level')
    procent_to_round = tables.Column(visible=False)
    price  = tables.Column()
    # def render_max_prnt(self, value):
    #     if value < 5:
    #         color = "red"
    #     else:
    #         color = "blue"
    #     return format_html("<font color='{}'>{}</font>", color, value)
    
    def render_round_price(self, value, record):
        color = "black"
        if record['procent_to_round'] > 0:
            color = "green"
            # html = format_html("{}:<font color='green'>{}</font>", value, record['procent_to_round'])
        else:    
            color = "red"
            # html = format_html("{}:<font color='red'>{}</font>", value, record['procent_to_round'])
        bold = ""
        if record['round_price']%5 == 0:
            bold = "bold"
        
        html = format_html("<span style='font-weight:{}'>{}</span>:<font color='{}'>{}</font>", bold, value, color, record['procent_to_round'] )

        return html
    class Meta:
        attrs = {"class": "s_table table-striped"}


def write_stats_to_file(stats,proccents):
    file = open('stats.txt','w')
    for s,p in zip(stats, proccents):
        file.write(str(s) + '\n')
        file.write(str(p) + '\n')
    file.close()

def stocks_table(request):    
    # stats_ans = client.query("SELECT * FROM statics WHERE time > now() - 1d", database='statics') 
    stats_ans = client.query("SELECT * FROM statics WHERE time > now() - 30m GROUP BY ticker", database='statics') 

    prices_ans = client.query("SELECT * FROM current_prices WHERE time > now() - 1d", database='current_prices')
    # print(prices_ans)
    if len(prices_ans) == 0:
        
        print('Текущих цен нет')
    # l1 = list(stats_ans)[0]
    # print(len(l1))
    print(len(list(stats_ans)), len(list(prices_ans)),)

    def calculate_procents(prices_ans,stats_ans):
        proccents = []
        stats     = []
        # tickers = [s['ticker'] for s in list(stats_ans)[0]]
        
        #Разбираем запрос
        last_stats = {}
        for s in stats_ans.raw['series']:
            d = {}
            for i, name in enumerate(s['columns']):
                d.update({name:s['values'][-1][i]})
            last_stats[s['tags']['ticker']] = d

        tickers = [s['tags']['ticker']  for s in stats_ans.raw['series']]
        for ticker in tickers:
            try:
                # c_stat = list(stats_ans.get_points(tags={'ticker':ticker}))[0]
                c_stat = last_stats[ticker]
                c_price = list(prices_ans.get_points(tags={'ticker':ticker}))[-1]
                # c_price = list(stats_ans.get_points(tags={'ticker':ticker}))[-1]
                # print(c_price)
            except Exception as exc:
                print(ticker, exc)
                # continue
            # print(c_stat)
            p = {'ticker'   : ticker,            
                'min_prnt' : round( (c_price['close']/c_stat['min'] - 1)*100, 2),
                'min_day'  : round( (c_price['close']/c_stat['min_day'] - 1)*100, 2),
                'min_week' : round( (c_price['close']/c_stat['min_week'] - 1)*100, 2),
                'min_month': round( (c_price['close']/c_stat['min_month'] - 1)*100, 2),

                'max_prnt' : round( (c_price['close']/c_stat['max'] - 1)*100, 2),
                'max_day'  : round( (c_price['close']/c_stat['max_day'] - 1)*100, 2),
                'max_week' : round( (c_price['close']/c_stat['max_week'] - 1)*100, 2),
                'max_month': round( (c_price['close']/c_stat['max_month'] - 1)*100, 2),
            }

            d = {'ticker'   : ticker,
                 'min_day'  : c_stat['min_day'],
                 'min_week' : c_stat['min_week'],
                 'min_month': c_stat['min_month'],
                 'min_prnt' : c_stat['min'],

                 'max_day'    : c_stat['max_day'],
                 'max_week'   : c_stat['max_week'], 
                 'max_month'  : c_stat['max_month'], 
                 'max_prnt'   : c_stat['max'],
                 'price'      : c_price['close'],
                 'round_price': c_stat['round_price'],
                 'procent_to_round': c_stat['procent_to_round']
                 }
            proccents+=[p]
            stats    +=[d]
        return proccents, stats
    
    proccents, stats = calculate_procents(prices_ans,stats_ans) 
    json_proccents =  json.dumps(proccents) 
    table = NameTable(stats)
    print(len(stats))
    write_stats_to_file(stats, proccents)
    return render(request, 'monitor/stocks_table.html', {'stats': json_proccents, 'table':table} )
