import sys
sys.path.append('../')
from candle import CandleStream, StreamManager
from subs import candle_subs_spb
import time
from market_model import Market_model

import threading
import time


class ThreadingExample(object):
    """ Threading example class
    The run() method will be started and it will run in the background
    until the application exits.
    """

    def __init__(self, interval=1):
        """ Constructor
        :type interval: int
        :param interval: Check interval, in seconds
        """
        self.interval = interval

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()                                  # Start the execution

    def run(self):
        """ Method that runs forever """
        run_stock_stream_and_statistics()

def run_stock_stream_and_statistics():
    stream = CandleStream(candle_subs_spb,[],[],is_db = True, period_db_update = 10)
    # stream.start()
    manager = StreamManager()
    manager.add('spb',stream, None)
    tickers = ['AAL','AAPL','ABBV','AMD','APA','ATRA','BA','BABA','BBBY','BBY','CARS','CAT','CCI','CCK','CCL','CHEF','CVX','DAL','DIS','DK','DKS','DLPH','DLX','DOV','DXC','DY','EA','EBAY','EDIT','ET','EXC','EXEL','F','GILD','GIS','GOSS','GPN','GT','GTX','HA','HAL','HP','IBN','LB','LM','M','MO','MOMO','MRNA','MS','MSFT','NEM','O','OIS','OLL','OSIS','OVV','OXY','PAYC','PBF','PBI','PCG','PFG','PPG','PYPL','PZZA','QCOM','RCL',"RDS.A",'SAGE','SAIL','SAVE','SBUX','SNA','SNY','SPG','T','TSM','UAA','ULTA','UNH','WBA','WCC','WELL','WFC','XOM','ZYNE']
    print()
    # tickers = ['AAL','AAPL','ABBV','AMD','APA','ATRA','BA','BABA','BBBY','BBY','CARS','CAT','CCI']
    print('Запуск потока котировок\n')
    while True:
        manager.check_streams()
        market = Market_model(tickers)
        market.update_tickers_statics()
        time.sleep(20)

    print('Поток упал')

if __name__ == "__main__":
    run_stock_stream_and_statistics()
    # stream = ThreadingExample(1)
    print('')


