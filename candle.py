import websocket
from threading import Thread

import time
import datetime
from FIGIS import Market_names
import json
from six import print_
from copy import copy
from market_downloader import Market_Downloader

import numpy as np
from subs import candle_subs_spb
from influxdb import InfluxDBClient

import re
from bs4 import BeautifulSoup
import requests

token = "t.wlnxazSuK6W--U6od2e6k778AW3nfY7MPm-1h93rzYXl6JIVPTnODfaeaJz1EjhfWl6XnJn2MCmIlqJPeeF6tg"

def to_candle_subscribe_json(figi, interval):
    return '''{ "event": "candle:subscribe", "figi": "%s", "interval": "%s" }''' % (figi, interval)


def to_orderbook_subscribe_json(figi, depth):
    return '''{ "event": "orderbook:subscribe", "figi": "%s", "depth": %s }''' % (figi, depth)


def to_instrument_info_subscribe_json(figi):
    return '''{ "event": "instrument_info:subscribe", "figi": "%s" }''' % (figi)

def to_print_err(ws,err):
    print('Ошибка чтения сокета')
    print(err)
    return

def callback_decider(event_string,
                     on_candle_event,
                     on_orderbook_event,
                     on_instrument_info_event):
    event_json = json.loads(event_string)
    event_type = event_json['event']
    event_payload = event_json['payload']

    if event_type == 'candle':
        on_candle_event(event_payload)
    elif event_type == 'orderbook':
        on_orderbook_event(event_payload)
    elif event_type == 'instrument_info':
        on_instrument_info_event(event_payload)
    else:
        raise Exception("unknown event type - %s" % event_type)


def subscribe_to(ws, candle_subs=(), orderbook_subs=(), instrument_info_subs=()):
    subscribtions_list = []

    subscribtions_list.extend(map(lambda x: to_candle_subscribe_json(x['figi'], x['interval']), candle_subs))

    subscribtions_list.extend(map(lambda x: to_orderbook_subscribe_json(x['figi'], x['depth']), orderbook_subs))

    subscribtions_list.extend(map(lambda x: to_instrument_info_subscribe_json(x['figi']), instrument_info_subs))

    for sub in subscribtions_list:
        # print_("sending: %s" % sub)
        ws.send(sub)

class Candle:
    def __init__(self, open_price=0, close_price=0, high=0, low=0, volume=0, market=0, time = 'T'):
        self.open = open_price
        self.close = close_price
        self.high = high
        self.low = low
        self.volume = volume
        self.time = time
        self.market = market
    
    def __eq__(self, other):
        if self.open == other.open and self.close == other.close and self.volume == other.volume:
            return True
        else:
            return False

    def __str__(self):
        return "{} o: {}, v: {}, t: {}".format(self.market, self.open,  self.volume, self.time.split('T')[-1])

class CandleMap():
    def __init__(self, number_of_market):
        self._number_of_markets = number_of_market
        self._candles_map = []
        self._candles_map.append([Candle() for _ in range(self._number_of_markets)])
        self._market_indexs = {}

    def set_Candle(self, indx_time, indx_market, candle):
        self._candles_map[indx_time][indx_market] = candle

    def add_Row_With_None(self):
        self._candles_map.append([Candle() for _ in range(self._number_of_markets)])
    
    def get_Candle_By_Market(self, market_name):
        pass

    def set_Market_Indexs(self,indexs):
        self._market_indexs = indexs

    def get_Volume_Of_Market(self, market):
        index = self._market_indexs[market]
        return [self._candles_map[i][index].volume for i in range(len(self._candles_map))]

    def get_Last_Volume_Of_Market(self, market, offset=0):
        index = self._market_indexs[market]
        return self._candles_map[-1-offset][index].volume

    def get_Opens_by_Market(self, market, num_last_candles = 5):
        index = self._market_indexs[market]
        if len(self._candles_map) > num_last_candles:
            start = len(self._candles_map) - num_last_candles
            # print(len(self._candles_map), start)
            arr = []
            for i in range(num_last_candles):
                # if self._candles_map[start + i][index].open == None:
                #     m = np.mean(self._candles_map[start:][index].open)
                #     arr.append(m)
                # else:
                    arr.append(self._candles_map[start + i][index].open)
            # return [self._candles_map[start + i][index].open for i in range(num_last_candles)]
            return arr
        else:
            return -1

    def get_current_close_prices(self):
        """
            Получить текущие значение цены закрытия свечи. Использование на сайте
        """
        prices = {}
        for ticker, index in self._market_indexs.items():
            close = self._candles_map[-1][index].close
            if close != 0:
                prices[ticker] = close
            else:
                N = len(self._candles_map) - 1
                while N>0:
                    close = self._candles_map[N][index].close
                    if close!= 0:
                        prices[ticker] = close
                        break
                    prices[ticker] = -1
                    N-=1
        return prices

class CandleStream(Thread):
    def __init__(self,  candle_subs, orderbook_subs, instrument_info_subs, is_db, period_db_update):
        """
            period_db_update: период добавления свечей в бд
        """
        Thread.__init__(self)
        self.is_db = is_db
        self._candle_subs = candle_subs
        self._orderbook_subs = orderbook_subs
        self._instrument_info_subs = instrument_info_subs
        self._number_of_markets = len(self._candle_subs)
        self.__market_candles = CandleMap(self._number_of_markets)
        self._market_names = Market_names()
        self._start_timestamp = time.time() - 7*60*60
        self._market_indexs = { self._market_names.get_name_by_figi(elem['figi']) : int(i%len(self._candle_subs) ) for i, elem in enumerate(self._candle_subs)}
        print(self._market_indexs)
        self.__market_candles.set_Market_Indexs(self._market_indexs)
        self._last_index = 0
        self.period_db_update = period_db_update
        self.shift = 0
        # self._candles_map.append([None for _ in range(self._number_of_markets)])
        # self.initialization_candles_history(15)
        self.last_market = 'YNDX'
        self.last_sp_update = time.time()
        url = 'wss://api-invest.tinkoff.ru/openapi/md/v1/md-openapi/ws'
        if is_db:
            self.last_db_update = time.time()
        
            self.db_client = InfluxDBClient(host='127.0.0.1', port=8086, username='kirilman', password='blue', database='current_prices')
            self.ws = websocket.WebSocketApp(
                    url=url,
                    header=["Authorization: Bearer %s" % token],
                    on_open=lambda ws: subscribe_to(ws, candle_subs, orderbook_subs, instrument_info_subs),
                    on_message=lambda ws, msg: callback_decider(msg, self.add_candle_to_db, print_, print_),
                    on_error=lambda ws, err: to_print_err(ws, err),
                    # ping_interval=5
                )
            print('Подключились к БД')
        else:
            self.ws = websocket.WebSocketApp(
                    url=url,
                    header=["Authorization: Bearer %s" % token],
                    on_open=lambda ws: subscribe_to(ws, candle_subs, orderbook_subs, instrument_info_subs),
                    on_message=lambda ws, msg: callback_decider(msg, self.addCandle, print_, print_),
                    on_error=lambda ws, err: to_print_err(ws, err),
                    # ping_interval=5
                )

    def initialization_candles_history(self, number_last_candles):
        market_loader = Market_Downloader()
        for i in range(number_last_candles):
            self.__market_candles.add_Row_With_None()
        self.shift = number_last_candles
        for ticker, index_market in self._market_indexs.items():
            now_date = datetime.datetime.now() - datetime.timedelta(hours=4)
            last_date = now_date - datetime.timedelta(minutes = number_last_candles)
            _from = last_date.isoformat()+'+04:00'
            to = now_date.isoformat() + '+04:00'
            candles = market_loader.get_market_candles(ticker, _from, to, '1min')
            if candles is not None:
                for k, candle in enumerate(candles[::-1]):
                    new_candle = Candle(candle.o, 
                                    candle.c, 
                                    candle.h,
                                    candle.l,
                                    candle.v,
                                    ticker)
                    self.__market_candles.set_Candle(number_last_candles-1 -k, index_market, new_candle)
                # print(ticker, ' ', len(candles), new_candle)
            time.sleep(0.03)
        self._last_index = number_last_candles

    def addCandle(self, json_obj):

        candle_timestamp = datetime.datetime.strptime(json_obj['time'],'%Y-%m-%dT%H:%M:%SZ').timestamp()
        # print(candle_timestamp, self._start_timestamp, self.shift)

        index_time = int((candle_timestamp - self._start_timestamp)/60) + self.shift #смещение из за инициализации историч. свечей
        market = self._market_names.get_name_by_figi(json_obj['figi'])

        index_market = self._market_indexs[market]
        if index_time<0:
            raise Exception("index_time<0") 

        if self._last_index != index_time:
            for i in range(index_time - self._last_index):
                self._last_index = index_time
                self.__market_candles.add_Row_With_None()
                # print(json_obj)
                
        candle = Candle(json_obj['o'], 
                        json_obj['c'], 
                        json_obj['h'],
                        json_obj['l'],
                        json_obj['v'],
                        market, json_obj['time'])
        # print(candle.volume,' ', candle.market)
        try:
            self.__market_candles.set_Candle(index_time, index_market, candle)
        except Exception as e:
            print('m ',index_market, ' t', index_time,' ',len(self.__market_candles))
            print('Error ',e)

    def add_candle_to_db(self, json_obj):
        self.addCandle(json_obj)
        current_time = time.time()
        # Парсинг SP500
        if (current_time - self.last_sp_update) > 60:
            try:
                self.download_sp500()
            except Exception as exc:
                print(exc)
            
            self.last_sp_update = current_time
        if (current_time - self.last_db_update) > self.period_db_update:
            self.last_db_update = current_time
            print(current_time)
            sql_body = []
            prices = self.__market_candles.get_current_close_prices()
            for ticker, price in prices.items():
                # print(ticker, price)
                body = {
                "measurement":"current_prices",
                "tags":{
                    "ticker":ticker,
                    # "interval":interval,
                },
                # "time": candle.time.isoformat('T'),
                "fields": {
                    "close": price,
                },}
                sql_body.append(body)
            self.db_client.write_points(sql_body)
            # print(sql_body)
        

    def download_sp500(self):
        """
            Парсинг значения SP500
        """
        def get_sp500():
            def get_html(url):
                page = requests.get(url)
                return page.text
            html = get_html(url = 'https://www.finanz.ru/indeksi/diagramma-realnogo-vremeni/s&p_500')
            page = BeautifulSoup(html)
            divs = page.find_all('div', {"class":"price"})
            a = divs[4].text.replace('\xa0','').replace(',','.')
            return float(a) 

        sql_body = []
        ticker = 'SP500'
        price = get_sp500()

        current_time = datetime.datetime.now() - datetime.timedelta(hours=7)
        body = {
        "measurement":"current_prices",
        "tags":{
            "ticker":ticker,
            # "interval":interval,
        },
        "time": current_time.isoformat('T'),
        "fields": {
            "close": price,
        },}
        sql_body.append(body)
        self.db_client.write_points(sql_body)
        print(sql_body)

    def print_market(self, file):
        file.write('\n')
        for ticker, index_market in self._market_indexs.items():
            print(ticker,' ', len(self.__market_candles._candles_map),' ',self._last_index )

            c = self.__market_candles._candles_map[self._last_index-1][index_market]
            file.write("{0}:{1:6.2f},{1:6.2f},{3:9}|".format(ticker,float(c.open), float(c.close) , c.time.split('T')[1]) )
        file.write('\n')

    def run(self):
        print('Start Thread')
        try:
            self.ws.run_forever()
        except KeyboardInterrupt:
            self.ws.close()
            raise
        
    def set_websocker(self,websoket):
        del self.ws
        self.ws = websocket
        self.ws.run_forever()

    def set_candle_map(self, other_stream):
        if self._candle_subs == other_stream._candle_subs:
            self.__market_candles = copy(other_stream.__market_candles)
            self._last_index = other_stream._last_index
            self._start_timestamp = other_stream._start_timestamp

    def __del__(self):
        print('Stream Destructor')
    
    def get_candle_map(self):
        return self.__market_candles


import gc
class StreamManager():
    def __init__(self):
        self.__dict__ = {}
    def add(self,name,stream, analyzer):
        self.__dict__[name] = {'stream':stream,'analyzer':analyzer}

    def check_streams(self):
        isFail = []
        for key,elem in self.__dict__.items():
            # print(key,elem)
            stream = elem['stream']
            # analyzer = elem['analyzer']
            if stream.ws.sock is None:
                isFail.append(key)
        
        for key in isFail:
            stream = self.__dict__[key]['stream']
            analyzer = self.__dict__[key]['analyzer']
            new_stream = CandleStream(stream._candle_subs, stream._orderbook_subs, stream._instrument_info_subs, stream.is_db, stream.period_db_update)
            new_stream.start()
            new_stream.set_candle_map(stream)
            del stream
            if analyzer:
                analyzer.setCandleMap(new_stream.get_candle_map())
            self.__dict__[key]['stream'] = new_stream
        gc.collect()

    def __getitem__(self, key):
        return self.__dict__[key]
    # def check(self):
    #     for stream, analyzer in zip(self.__streams, self.__analyzers):
    #         if stream.ws.sock is None:
    #             new_soket = websocket.WebSocketApp(
    #                     url= 'wss://api-invest.tinkoff.ru/openapi/md/v1/md-openapi/ws',
    #                     header=["Authorization: Bearer %s" % token],
    #                     on_open=lambda ws: subscribe_to(ws, stream._candle_subs, stream._orderbook_subs, stream._instrument_info_subs),
    #                     on_message=lambda ws, msg: callback_decider(msg, stream.addCandle, print_, print_),
    #                     on_error=lambda ws, err: to_print_err(ws, err),
    #                     )
    #             stream.set_websocker(new_soket)
    #             print(stream," перезапущен" )



# stream = CandleStream(candle_subs_spb,[],[], is_db = True, period_db_update = 30)
# stream.start()

# manager = StreamManager()
# manager.add('spb',stream, None)
# f = open('market.txt','w',buffering=2)
# while True:
#     # stream.print_market(f)
#     # manager['spb']['stream'].print_market(f)
#     p = manager['spb']['stream'].get_candle_map().get_current_close_prices()
#     # print(p)
#     time.sleep(5)
#     manager.check_streams()
    
# f.close('market.txt')