from PyQt5 import QtWidgets, QtCore
import sys
from functools import partial
import numpy as np
import os
# os.environ['QT_QPA_PLATFORM']='offscreen'
from ui import Ui_MainWindow  # импорт нашего сгенерированного файла
# from grid_ui import Ui_MainWindow
from reader import Reader
from canvas_figure import Canvas
from candle import CandleStream, StreamManager

from analyzer import StockAnalyzer
from time import sleep
import asyncio

from subs import candle_subs, candle_subs_15, candle_subs_spb, candle_subs_spb_15
orderbook_subs, instrument_info_subs = [], []

from functools import partial


def create_simple_figure(fig, color='red'):
    axes = fig.gca()
    t = np.linspace(0, 10)
    b = np.random.randint(-3, 3)
    print(b)
    axes.plot(t, b*t**2, color=color)
    return 


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.canvas = Canvas()
        self.iter = 0
        self.number_last_candles = 15
       
        #Кореляционная карта
        self.frame = Reader().read_frame('resource/MOEX_TOP.csv', sep=',', index_col='TIME')
        self.ui.slider.setMinimum(self.frame.month[0])
        self.ui.slider.setMaximum(self.frame.month[-1])
        self.ui.label_begin.setText(self.frame.index[0].isoformat().split('T')[0])
        self.ui.label_end.setText(self.frame.index[-1].isoformat().split('T')[0])
        self.ui.slider.valueChanged[int].connect(self.redraw_correlation_figure)

        self.ui.button.clicked.connect(self.on_click)

        #Потоки для получения котировок
        self.create_streams()
        self.stream_manager['moex_3']['analyzer'].signal_change_stocks.connect(self.update_table_with_stocks)
        # self.stream_manager['spb_1']['analyzer'].signal_get_trend_angles.connect( partial(self.show_trend_angles,5))
        self.stream_manager['spb_1']['analyzer'].signal_get_trend_angles.connect( self.show_trend_angles)

        f = open('market.txt','w', buffering=20)
        #Таймеры
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(partial(self.redraw_volums, number_last_candles = self.number_last_candles, out = f))
        self.timer.start(60000)

        self.timer_analyzer = QtCore.QTimer()
        self.timer_analyzer.timeout.connect(self.stream_manager['moex_1']['analyzer'].analyze_volumns)
        self.timer_analyzer.start(1000)

        self.timer_analyzer_spb = QtCore.QTimer()
        self.timer_analyzer_spb.timeout.connect(self.stream_manager['spb_1']['analyzer'].analyze_volumns)
        self.timer_analyzer_spb.start(1000)

        self.timer_analyzer_angel = QtCore.QTimer()
        self.timer_analyzer_angel.timeout.connect(partial(self.stream_manager['spb_1']['analyzer'].get_trend_angles, 5))
        self.timer_analyzer_angel.start(1000)

        self.timer_stocks = QtCore.QTimer()
        self.timer_stocks.timeout.connect(self.stream_manager['moex_3']['analyzer'].get_daily_change)
        self.timer_stocks.start(5000)
        self.stream_manager['moex_1']['analyzer'].signal_change_volums.connect(self.append_to_edit)
        self.stream_manager['spb_1']['analyzer'].signal_change_volums.connect(self.append_to_edit)

        len(candle_subs_spb_15)
       #Фигуры для отрисовки объемов
        self.figures = []
        for i, name in enumerate(self.stream_manager['moex_1']['stream']._market_indexs):
            volums = self.stream_manager['moex_1']['stream'].get_candle_map().get_Volume_Of_Market(name)
            fig = Canvas(4,4, 80)
            fig.plot_volume_bar(volums)
            self.figures.append(fig)
            if i>4:
                self.ui.horizontalLayout1.addWidget(fig)
            else:
                self.ui.horizontalLayout.addWidget(fig)
            if i>8:
                break

    def redraw_correlation_figure(self,value):
        mask = self.frame.month == value
        corr = self.frame.drop('month', axis=1)[mask].corr().values
        current_date = self.frame.drop('month', axis=1)[mask].index[0]
        self.ui.label_current_date.setText(current_date.isoformat().split('T')[0])
        names = self.frame.columns.drop('month')
        self.ui.correlation_figure.plot_correlation_map(corr,names, current_date)
        
        # print(self.frame.drop('month', axis=1)[mask])
		
    def on_click(self):
        self.ui.text_edit.clear()

    def append_to_edit(self, input_text):
        self.ui.text_edit.appendPlainText(input_text)

    def redraw_volums(self, number_last_candles,out):
        self.stream_manager.check_streams()
        try:
            volumns_ynd = self.stream_manager['moex_1']['stream'].get_candle_map().get_Volume_Of_Market('YNDX')
            for name, fig in zip(self.stream_manager['moex_1']['stream']._market_indexs, self.figures):
                volumns = self.stream_manager['moex_1']['stream'].get_candle_map().get_Volume_Of_Market(name)
                if len(volumns) == 0:
                    continue
                fig.plot_volume_bar(volumns[-number_last_candles:], name)
            self.iter+=1
        except Exception as ex:
            print('Неудалось перерисовать объемы ',ex)

        self.stream_manager['spb_1']['stream'].print_market(out)


        # print(self.iter, volumns[-1], volumns_ynd)
        # print(self.candle_stream.ws.sock.getstatus())
       
    def update_table_with_stocks(self,growing_stocks, falling_stocks):
        # for candle in self.candle_stream_15._candles_map._candles_map[0][:5]:
            # print(candle.__str__())
        # print(growing_stocks, falling_stocks)
        for ind, ((market, change) ,(market_fal, change_fal)) in enumerate(zip(growing_stocks,falling_stocks)):
            # print(type(change),change)
            self.ui.table_stocks.setItem(ind,0, QtWidgets.QTableWidgetItem(market))
            self.ui.table_stocks.setItem(ind,1, QtWidgets.QTableWidgetItem(str(np.round(change,2))))
            self.ui.table_stocks.setItem(ind,2, QtWidgets.QTableWidgetItem(market_fal))
            self.ui.table_stocks.setItem(ind,3, QtWidgets.QTableWidgetItem(str(np.round(change_fal,2))))

    def show_trend_angles(self, pos_angles, neg_angles):
        """
            Отобразить углы тренда в таблице
        """
        # print(len(pos_angles), len(neg_angles))
        for row, ((market, pos_angle), (market_neg, neg_angle)) in enumerate(zip(pos_angles, neg_angles)):
            self.ui.table_stocks.setItem(row, 8, QtWidgets.QTableWidgetItem(market))
            self.ui.table_stocks.setItem(row, 9, QtWidgets.QTableWidgetItem( str(round(pos_angle,2))) )
            self.ui.table_stocks.setItem(row, 10, QtWidgets.QTableWidgetItem(market_neg))
            self.ui.table_stocks.setItem(row, 11, QtWidgets.QTableWidgetItem( str(round(neg_angle,2))) )





    def create_streams(self):
        self.stream_manager = StreamManager()
        
        stream = CandleStream(candle_subs, orderbook_subs, instrument_info_subs, False, 100)
        # stream = CandleStream(candle_subs_spb_15, orderbook_subs, instrument_info_subs)
        stream.start()
        stock_analyzer = StockAnalyzer(stream.get_candle_map(), 'MOEX')

        self.stream_manager.add('moex_1', stream, stock_analyzer)

        stream_15 = CandleStream(candle_subs_15, orderbook_subs, instrument_info_subs,   False, 100)
        stream_15.start()
        stock_analyzer = StockAnalyzer(stream_15.get_candle_map(), 'MOEX')
        self.stream_manager.add('moex_3', stream_15, stock_analyzer)
        
        stream = CandleStream(candle_subs_spb, orderbook_subs, instrument_info_subs,  False, 100)
        stream.initialization_candles_history(11)
        stream.start()

        stock_analyzer = StockAnalyzer(stream.get_candle_map(), 'SPB')
        self.stream_manager.add('spb_1', stream, stock_analyzer)
        
        

    
    
    def stream_checker(self):
        streams = self.streams.copy()
        for stream in streams:
            stream
        pass


if __name__ == "__main__":
    # stream = CandleStream([candle_subs_spb[0]], [], [])
    # stream.start()

    app = QtWidgets.QApplication([])
    application = MainWindow()
    application.show()
    sys.exit(app.exec())


    # while True:
    #     print('Start')
    # st1 = CandleStream(candle_subs[:2],[],[])
    # st2 = CandleStream(candle_subs[:2],[],[])
    # st1.start()
    # st2.start()
    # anl = StockAnalyzer(st1._candles_map)
    # manager = StreamManager()
    # manager.add('SPB_1', st1, anl)
    # manager.add('MOEX', st2, anl)
    # # manager.add('SPB_1', st1, -1)
    # manager.check_streams()
    # print(manager.dict['SPB_1'])