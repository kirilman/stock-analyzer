from candle import CandleMap, Candle
import pandas as pd
import numpy as np
from datetime import datetime
from PyQt5 import QtCore
from datetime import datetime
import time
# from downloader_finam import has_last_stocks, get_last_stocks,get_last_day
from FIGIS import Market_names
from sklearn.linear_model import LinearRegression as LR
from market_downloader import Market_Downloader

stock_previous = {}
class StockLastDay():
    def __init__(self, tickers):
        down_loader = Market_Downloader()
        # path = get_last_day(tickers,Market_names())
        path = down_loader.get_last_day(tickers, Market_names())
        print(path)
        print("Скачано")
        self.stocks = {}
        print(len(tickers),' in last day')
        for ticker in tickers:
            # print(ticker)
            try:
                f = open(path+ticker+'.txt','r')
                # print(f)
                first_line = f.readline()
                # print(first_line)
                if len(first_line) == 0:
                    f.close()
                    continue
                read_line = f.readline()
                items = read_line.split(';')
                    
                self.stocks[ticker] = Candle(float(items[0]), float(items[3]), float(items[1]),
                                        float(items[2]),float(items[4]),market = ticker)
                f.close()
            except:
                print('Неудалось открыть файл '+path+ticker+'.txt')


class StockAnalyzer(QtCore.QObject):

    signal_change_volums = QtCore.pyqtSignal(str)
    #signal_сhange_stocks = QtCore.pyqtSignal(str)
    signal_change_stocks = QtCore.pyqtSignal(list, list)
    signal_get_trend_angles    = QtCore.pyqtSignal(list, list)
    def __init__(self, candle_map, exchange):
        super(StockAnalyzer, self).__init__()
        self.candles = candle_map
        self.tresholds = {}
        if exchange == 'MOEX':
            frame = pd.read_csv('resource/MOEX_Threshold.csv', sep=';')
        else:
            frame = pd.read_csv('resource/SPB_Threshold.csv', sep=';')

        for line in frame.itertuples():
            # print(line)
            self.tresholds.update({line[1]:[line[2], line[3]] }) 

        self.is_show = np.zeros(self.candles._number_of_markets, dtype=bool)
        self.last_minute = datetime.now().minute
        tickers = [x[0] for x in self.candles._market_indexs.items()]
        try:
            tickers.remove(None)
        except:
            pass
        # print(tickers)
        self.last_day = StockLastDay(tickers)

    def update_is_show(self):
        cur_minute = datetime.now().minute
        if self.last_minute != cur_minute:
            # print(self.last_minute, cur_minute)
            self.last_minute = cur_minute
            self.is_show = self.is_show*False
        
    def analyze_volumns(self):
        # print(self.is_show)
        # print('test')
        start_time = time.time()
        # self.get_daily_change()
        try:
            if len(self.candles._candles_map) < 3:
                return 
            for market, index in self.candles._market_indexs.items():
                # print(market, index)
                if not self.is_show[index]:
                    last = self.candles.get_Last_Volume_Of_Market(market, 0)
                    pre_last = self.candles.get_Last_Volume_Of_Market(market, 1)
                    if (last < 0.000001) or (pre_last<0.000001):
                        continue
                    one = np.log(last)
                    two = np.log(pre_last)
                    # print(one, two, self.tresholds[market], self.tresholds[market],'\n')
                    if one > self.tresholds[market][1] and two > self.tresholds[market][1]:
                        cur_time = datetime.now().strftime('%H:%M:%S')
                        text = '{} {}: Тип две свечи'.format(cur_time, market)
                        self.is_show[index] = True
                        self.signal_change_volums.emit(text)

                    if one > self.tresholds[market][0]:
                        cur_time = datetime.now().strftime('%H:%M:%S')
                        sigma = (1 - self.tresholds[market][0]/one ) * 100
                        text = '{} {}: Тип одна свеча, отклонение {:.3f}%'.format(cur_time, market,sigma)
                        # print(text)
                        self.is_show[index] = True
                        self.signal_change_volums.emit(text)
            self.update_is_show()
            # print('Время выполнения: %.3f' % float(time.time() - start_time)) 
        except Exception as exc:
            print('Неудалось проанализировать обьем', exc)

    def setCandleMap(self, candle_map):
        self.candles = candle_map

    def get_daily_change(self):
        changes = {}
        N =  80
        try:
            for name, index in self.candles._market_indexs.items():
                current_open = self.candles._candles_map[-1][index].open 
                if name in self.last_day.stocks:
                    last_close = self.last_day.stocks[name].close
                res = (current_open/last_close - 1)*100
                if res == -100:
                    continue
                changes[name] = res
            growing_stocks = sorted(changes.items(), key=lambda kv: kv[1],reverse=True)[:N]
            falling_stocks = sorted(changes.items(), key=lambda kv: kv[1],reverse=False)[:N]

            self.signal_change_stocks.emit(growing_stocks, falling_stocks)
        except Exception as e:
            print(e)
            self.signal_change_stocks.emit([], [])

    def get_trend_angles(self, num_candles=10):
        """
            num_candles: число свечей для построения линии
        """
        def angle_between_lines(s1, s2):
            """
                return angs: в градусах
            """
            if len(s1) != len(s2):
                return 0
            T = np.arange(0, len(s2)).reshape(-1,1)
            model_1 = LR().fit(T, s1.reshape(-1,1))
            model_2 = LR().fit(T, s2.reshape(-1,1))
            k1 = model_1.coef_[0][0]
            k2 = model_2.coef_[0][0]
            arg = (k2-k1)/(1+k1*k2)
            angle = np.arctan(arg)*180/np.pi # в градусах
            return angle

        try:
            angles = {}
            for ticker, _ in self.candles._market_indexs.items():
                last_opens = np.array(self.candles.get_Opens_by_Market(ticker,num_candles))
                
                if isinstance(last_opens, int):
                    continue
                # print(last_opens,num_candles)
                if len(last_opens) < num_candles:
                    continue

                angle = angle_between_lines(np.zeros((num_candles,1)), last_opens)
                if angle > 85. or angle < -80.:
                    continue
                else:
                    angles[ticker] = angle
              
              
            pos_angles = sorted(angles.items(), key = lambda kv: kv[1], reverse=False)[:35]
            neg_angles = sorted(angles.items(), key = lambda kv: kv[1], reverse=True) [:35]
          
            # print(pos_angles)
            # print(neg_angles)
            self.signal_get_trend_angles.emit( pos_angles, neg_angles)
        except Exception as exc:
            print("Углы")
            print(exc)
            pass