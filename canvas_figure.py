
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas, NavigationToolbar2QT
from matplotlib.figure import Figure

# plt.style.use('fivethirtyeight')
SMALL_SIZE = 10
MEDIUM_SIZE = 11
class Canvas(FigureCanvas):
    def __init__(self, width=4, height=4, dpi=85):
        plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
        plt.rc('axes', labelsize=MEDIUM_SIZE)
        plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
        plt.rc('ytick', labelsize=SMALL_SIZE)
        self.fig = Figure( figsize=(width, height), dpi = dpi)
        self.axes = self.fig.add_subplot(111)
        # plt.tight_layout()
        FigureCanvas.__init__(self, self.fig)
        FigureCanvas.updateGeometry(self)
        self.colorbar = None
        # create_simple_figure(self.fig)   
        # FigureCanvas.updateGeomerty(self)

    def plot_volume_bar(self, volumes, name='Объемы'):
        self.axes.cla()

        self.axes.bar(range(len(volumes)), volumes, width = 0.85)
        # self.axes.set_yticklabels(self.axes.get_yticklabels(), rotation = 90)
        self.axes.tick_params(axis='y', labelrotation=90)
        # plt.tight_layout(pad=10.01)
        self.axes.set_title(name)
        self.fig.tight_layout(h_pad=0.6)
        # self.axes.axis('equal')
        self.draw()

    def plot_correlation_map(self, correlation, names, date):
        self.axes.cla()
        if self.colorbar:
            self.colorbar.remove()
        # self.fig.delaxes(self.fig.axes[0])

        im = self.axes.imshow(correlation,vmin=-1, vmax=1, cmap='RdYlBu', label=date)
        # cb = self.fig.colorbar(im) 
        # cb.ax.clear()
        self.colorbar = self.fig.colorbar(im, ax=self.axes)
        # self.axes.legend()
        self.axes.set_yticks(range(len(names)))
        self.axes.set_yticklabels(names);
        self.axes.set_xticks(range(len(names)))
        self.axes.set_xticklabels(names,rotation = 45);
        self.fig.tight_layout(pad=0.05)
        self.draw()
        # self.colorbar.ax.clear()